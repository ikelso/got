﻿$GetCharacters = New-UDEndpoint -Url "/got/:character" -Method "GET" -Endpoint {
    param($character)

    $ApiResponse = @()
    ForEach ($member in $cache:Characters) {
        if ($member.Name -like "*$character*") {
            $ApiResponse += $member } 
            }
        $json = $ApiResponse | ConvertTo-Json
        return $json
    }

$GetHouses = New-UDEndpoint -Url "/got/house" -Method "GET" -Endpoint {
    $ApiResponse = ($cache:houses | ConvertTo-Json)
    return $ApiResponse
    }

 
$LoadData = New-UDEndpoint -Url "/got/import" -Method "POST" -Endpoint {
    param($Body)
    try {
        $localData = $Body | ConvertFrom-Json
        $cache:Characters = $localData.Characters
        $cache:Houses = $localData.House
        $ApiResponse = ($cache:Houses | ConvertTo-Json)
    } catch {
        $ApiResponse = "Unable to load data: $($_.exception)"
    }
    return $ApiResponse
}

$endpoints = @()
$endpoints += $LoadData
$endpoints += $GetCharacters
$endpoints += $GetHouses


Start-UDRestApi -port 8088 -Endpoint $endpoints

